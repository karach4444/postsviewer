//
//  RequestAuthManager.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 2.03.21.
//

import Foundation
import Alamofire
import Combine

class RequestAuthManager: RequestInterceptor {
    private let authManager: AuthManagerProtocol
    private var requestsForAuthorization: [(URLRequest, (Result<URLRequest, Error>) -> ())]
    private var requestsForRetry: [(RetryResult) -> ()]
    private var subsriptions: Set<AnyCancellable>
    
    init(authManager: AuthManagerProtocol) {
        self.authManager = authManager
        self.requestsForAuthorization = []
        self.requestsForRetry = []
        self.subsriptions = []
        subscribeToAuthChanges()
    }
    
    public func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        DispatchQueue.main.async {
            self.adapt(urlRequest, completion: completion)
        }
    }
    
    private func adapt(_ urlRequest: URLRequest, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        switch authManager.state {
        case .authenticated(let token, _):
            let modifiedRequest = authorize(request: urlRequest, with: token)
            completion(.success(modifiedRequest))
        case .refreshing:
            self.requestsForAuthorization.append((urlRequest, completion))
        case .notAuthentificated:
            let error = RequestAuthManagerError.notAuthenticated
            completion(.failure(error))
        case .initial:
            completion(.success(urlRequest))
        }
    }
    
    public func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        DispatchQueue.main.async {
            self.retry(request, for: session, dueTo: error, completion: completion)
        }
    }
    
    private func retry(_ request: Request, completion: @escaping (RetryResult) -> Void) {
        guard let statusCode = request.response?.statusCode, statusCode == Constants.authErrorStatusCode else {
            completion(.doNotRetry)
            return
        }
        switch authManager.state {
        case .authenticated(let currentToken, _):
        if let failedToken = request.request?.headers.value(for: Keys.authorization.rawValue),
           failedToken == bearerToken(from: currentToken) {
            requestsForRetry.append(completion)
            authManager.refreshAuthInfo()
        } else {
            completion(.retry)
        }
        case .refreshing:
            requestsForRetry.append(completion)
        case .notAuthentificated:
            let error = RequestAuthManagerError.notAuthenticated
            completion(.doNotRetryWithError(error))
        case .initial:
            completion(.doNotRetry)
        }
    }
    
    private func subscribeToAuthChanges() {
        authManager
            .statePublisher
            .subscribe { [weak self] state in
                guard let self = self else { return }
                switch state {
                case .authenticated(let token, _):
                    self.requestsForAuthorization.forEach({ request, completion in
                        let modifiedRequest = self.authorize(request: request, with: token)
                        completion(.success(modifiedRequest))
                    })
                    self.requestsForRetry.forEach({ completion in
                        completion(.retry)
                    })
                case .notAuthentificated:
                    let error = RequestAuthManagerError.notAuthenticated
                    self.requestsForAuthorization.forEach({ _, completion in
                        completion(.failure(error))
                    })
                    self.requestsForRetry.forEach({ completion in
                        completion(.doNotRetryWithError(error))
                    })
                case .initial, .refreshing:
                    return
                }
            }
            .store(in: &subsriptions)
    }
    
    private func authorize(request: URLRequest, with token: String) -> URLRequest {
        let bearerToken = self.bearerToken(from: token)
        let authHeader = HTTPHeader(name: Keys.authorization.rawValue, value: bearerToken)
        var modifiedRequest = request
        modifiedRequest.headers.add(authHeader)
        return modifiedRequest
    }
    
    private func bearerToken(from token: String) -> String {
        return "\(Keys.bearer.rawValue) \(token)"
    }
    
    private enum Keys: String {
        case authorization = "Authorization"
        case bearer = "Bearer"
    }
    
    private enum Constants {
        static let authErrorStatusCode = 401
    }
}

enum RequestAuthManagerError: LocalizedError {
    case notAuthenticated
    
    var errorDescription: String? {
        switch self {
        case .notAuthenticated:
            return "Not Authenticated"
        }
    }
}
