//
//  AuthManager.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 1.03.21.
//

import Foundation
import Combine
import KeychainSwift

protocol AuthManagerProtocol {
    var state: AuthState { get }
    var statePublisher: AnyPublisher<AuthState, Never> { get }
    
    func autologin()
    func authenticate(login: String, password: String, completion: @escaping (Result<Void, APIError>) -> ())
    func refreshAuthInfo()
    func logout()
}

enum AuthState {
    case initial
    case refreshing
    
    case authenticated(token: String, source: AuthentificationSource)
    enum AuthentificationSource {
        case login
        case autologin
        case tokenRefresh
    }
    
    case notAuthentificated(source: NotAuthenticatedSource)
    enum NotAuthenticatedSource {
        case initial
        case logout
        case autologinError(Error)
        case tokenRefreshError(Error)
    }
}

class AuthManager: AuthManagerProtocol {
    private let apiService: AuthAPIService
    private let keychain: KeychainSwift
    private let stateSubject: CurrentValueSubject<AuthState, Never>
    private var subscriptions: Set<AnyCancellable>
    
    var state: AuthState {
        return stateSubject.value
    }
    var statePublisher: AnyPublisher<AuthState, Never> {
        return stateSubject.eraseToAnyPublisher()
    }
    
    init(apiService: AuthAPIService) {
        self.apiService = apiService
        self.keychain = KeychainSwift()
        self.stateSubject = CurrentValueSubject<AuthState, Never>(.initial)
        self.subscriptions = []
    }
    
    func autologin() {
        guard let authInfo = savedAuthInfo() else {
            stateSubject.send(.notAuthentificated(source: .initial))
            return
        }
        stateSubject.send(.refreshing)
        apiService
            .authenticate(login: authInfo.login, password: authInfo.password)
            .subscribe { [weak self] result in
                guard let self = self else { return }
                let newState: AuthState = .authenticated(token: result.token, source: .autologin)
                self.stateSubject.send(newState)
            } onFailure: { [weak self] error in
                let newState = AuthState.notAuthentificated(source: .autologinError(error))
                self?.stateSubject.send(newState)
            }
            .store(in: &subscriptions)
    }
    
    func authenticate(login: String, password: String, completion: @escaping (Result<Void, APIError>) -> ()) {
        apiService
            .authenticate(login: login, password: password)
            .subscribe { [weak self] result in
                guard let self = self else { return }
                let authInfo = AuthInfo(login: login, password: password)
                self.saveAuthInfo(authInfo)
                let newState = AuthState.authenticated(token: result.token, source: .login)
                self.stateSubject.send(newState)
                completion(.success(()))
            } onFailure: { error in
                completion(.failure(error))
            }
            .store(in: &subscriptions)
    }
    
    func refreshAuthInfo() {
        guard let authInfo = savedAuthInfo() else {
            let error = BasicError("Auth Info is missing")
            let newState = AuthState.notAuthentificated(source: .tokenRefreshError(error))
            stateSubject.send(newState)
            return
        }
        
        stateSubject.send(.refreshing)
        apiService
            .authenticate(login: authInfo.login, password: authInfo.password)
            .subscribe { [weak self] result in
                guard let self = self else { return }
                let newState = AuthState.authenticated(token: result.token, source: .tokenRefresh)
                self.stateSubject.send(newState)
            } onFailure: { [weak self] error in
                let newState = AuthState.notAuthentificated(source: .tokenRefreshError(error))
                self?.stateSubject.send(newState)
            }
            .store(in: &subscriptions)
    }
    
    func logout() {
        cleanAuthInfo()
        let newState = AuthState.notAuthentificated(source: .logout)
        stateSubject.send(newState)
    }
    
    private func saveAuthInfo(_ authInfo: AuthInfo) {
        guard let data = try? JSONEncoder().encode(authInfo) else {
            return
        }
        keychain.set(data, forKey: Keys.authInfo.rawValue, withAccess: .accessibleAfterFirstUnlock)
    }
    
    private func savedAuthInfo() -> AuthInfo? {
        if let data = keychain.getData(Keys.authInfo.rawValue), let authInfo = try? JSONDecoder().decode(AuthInfo.self, from: data) {
            return authInfo
        } else {
            return nil
        }
    }
    
    private func cleanAuthInfo() {
        keychain.delete(Keys.authInfo.rawValue)
    }
    
    private enum Keys: String {
        case authInfo = "AuthManager.AuthInfo"
    }
}


