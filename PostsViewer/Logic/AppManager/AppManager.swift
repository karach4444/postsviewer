//
//  AppManager.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 2.03.21.
//

import Foundation

protocol AppManagerProtocol {
    var authManager: AuthManagerProtocol { get }
    var sessionManager: SessionManagerProtocol { get }
}

class AppManager: AppManagerProtocol {
    let authManager: AuthManagerProtocol
    let sessionManager: SessionManagerProtocol
    
    init() {
        let authApiService = AuthAPIService(sessionManager: SessionManager())
        self.authManager = AuthManager(apiService: authApiService)
        self.sessionManager = SessionManager(requestInterceptor: RequestAuthManager(authManager: authManager))
    }
}
