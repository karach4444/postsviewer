//
//  TodosAPIService.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import Foundation
import Combine

class TodosAPIService: BaseAPIService {
    func fetchTodos() -> AnyPublisher<[Todo], APIError> {
        sessionManager.requestDecodable(APIRequest<NoParameters>(path: "todos"), decodedType: [Todo].self)
    }
}
