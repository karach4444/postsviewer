//
//  KeypathOperators.swift
//  ArchitectureExample
//
//  Created by Nick Sudibor on 15.02.21.
//

import Foundation
import Combine

extension Publisher {
    func data(at keypath: String?) -> Publishers.TryMap<Self, Data> where Output == Data {
        tryMap { (data: Data) -> Data in
            guard let keypath = keypath else { return data }
            
            var json = try JSONSerialization.jsonObject(with: data, options: [])
            for key in keypath.components(separatedBy: "/") {
                guard let dictionatyJSON = json as? [String: Any] else {
                    throw BasicError("Could not cast json to [String: Any] for key '\(key)'")
                }
                if let value = dictionatyJSON[key], !(value is NSNull) {
                    json = value
                } else {
                    throw BasicError("Could not find value for key '\(key)'")
                }
            }
            let resultData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return resultData
        }
    }
    
    func optionalData(at keypath: String?) -> Publishers.TryMap<Self, Data?> where Output == Data? {
        tryMap { (optionalData: Data?) -> Data? in
            guard let data = optionalData else { return nil }
            guard let keypath = keypath else { return data }
            
            var json = try JSONSerialization.jsonObject(with: data, options: [])
            for key in keypath.components(separatedBy: "/") {
                guard let dictionatyJSON = json as? [String: Any] else {
                    throw BasicError("Could not cast json to [String: Any] for key '\(key)'")
                }
                if let value = dictionatyJSON[key], !(value is NSNull) {
                    json = value
                } else {
                    return nil
                }
            }
            let resultData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return resultData
        }
    }
}
