//
//  APIError.swift
//  ArchitectureExample
//
//  Created by Nick Sudibor on 15.02.21.
//

import Foundation
import Alamofire

enum APIError: LocalizedError {
    case networkConnectivity(URLError)
    case authentification(Error)
    case server(ServerError)
    case other(Error)
    
    init(rootError: Error, failedResponse: HTTPURLResponse?, responseData: Data?) {
        if let statusCode = failedResponse?.statusCode,
           statusCode == Constants.authenticationErrorStatusCode || statusCode == Constants.authorizationErrorStatusCode {
            self = .authentification(rootError)
        } else if let data = responseData, let serverError = try? JSONDecoder().decode(ServerError.self, from: data) {
            self = .server(serverError)
        } else if let afError = rootError as? AFError,
                  case .sessionTaskFailed(let nestedError) = afError,
                  let urlError = nestedError as? URLError {
            let code = urlError.code
            if code == .notConnectedToInternet || code == .networkConnectionLost {
                self = .networkConnectivity(urlError)
            } else {
                self = .other(rootError)
            }
        } else {
            self = .other(rootError)
        }
    }
    
    enum Constants {
        static let authenticationErrorStatusCode = 401
        static let authorizationErrorStatusCode = 403
    }
    
    
    var errorDescription: String? {
        switch self {
        case .networkConnectivity:
            return L10n.General.connectionErrorDescription
        case .server(let serverError):
            return serverError.message
        case .authentification, .other:
            return L10n.General.generalErrorDescription
        }
    }
}

// MARK: - Helpers

extension APIError {
    var isNetworkConnectivity: Bool {
        if case .networkConnectivity = self {
            return true
        } else {
            return false
        }
    }
    
    var isAuthentication: Bool {
        if case .authentification = self {
            return true
        } else {
            return false
        }
    }
    
    var isServer: Bool {
        if case .server = self {
            return true
        } else {
            return false
        }
    }
    
    var isOther: Bool {
        if case .other = self {
            return true
        } else {
            return false
        }
    }
}

// MARK: - Server Error

struct ServerError: Codable {
    let message: String
}
