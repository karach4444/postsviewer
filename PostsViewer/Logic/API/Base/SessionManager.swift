//
//  SessionManager.swift
//  ArchitectureExample
//
//  Created by Nick Sudibor on 15.02.21.
//

import Foundation
import Alamofire
import Combine

protocol SessionManagerProtocol {
    func request(_ request: URLRequestConvertible) -> AnyPublisher<Void, APIError>
    func requestData(_ request: URLRequestConvertible) -> AnyPublisher<Data?, APIError>
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String?, decoder: JSONDecoder, decodedType: ResultType.Type) -> AnyPublisher<ResultType, APIError>
    
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String?, decoder: JSONDecoder, decodedType: ResultType.Type) -> AnyPublisher<ResultType?, APIError>
    
    func uploadData(_ data: Data, request: URLRequestConvertible, progressHandler: @escaping Request.ProgressHandler) -> (UploadRequest, AnyPublisher<Data?, APIError>)
    func multipartUploadData(_ multipartFormData: MultipartFormData, request: URLRequestConvertible, progressHandler: @escaping Request.ProgressHandler) -> (UploadRequest, AnyPublisher<Data?, APIError>)
    
    func dowloadFile(at url: URL, destination: DownloadRequest.Destination?, progressHandler: @escaping Request.ProgressHandler) -> (DownloadRequest, AnyPublisher<URL, APIError>)
}

class SessionManager: SessionManagerProtocol {
    private let session: Session
    private let serializationQueue: DispatchQueue
    
    init(requestInterceptor: RequestInterceptor? = nil) {
        session = Session(interceptor: requestInterceptor)
        serializationQueue = DispatchQueue(label: "SessionManager.SerializationQueue", qos: .userInitiated, attributes: [.concurrent])
    }
    
    // MARK: - Plain
    
    private func sendRequest(_ request: URLRequestConvertible) -> AnyPublisher<Data?, APIError> {
        Future { fullfill in
            self.session
                .request(request)
                .validate()
                .response(queue: self.serializationQueue, completionHandler: { response in
                    switch response.result {
                    case .success(let data):
                        fullfill(.success(data))
                    case .failure(let error):
                        let apiError = APIError(rootError: error, failedResponse: response.response, responseData: response.data)
                        fullfill(.failure(apiError))
                    }
                })
        }.eraseToAnyPublisher()
    }
    
    func request(_ request: URLRequestConvertible) -> AnyPublisher<Void, APIError> {
        sendRequest(request)
            .map { _ in return () }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func requestData(_ request: URLRequestConvertible) -> AnyPublisher<Data?, APIError> {
        sendRequest(request)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    // MARK: - Decodable
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible,
                                                 keypath: String?,
                                                 decoder: JSONDecoder,
                                                 decodedType: ResultType.Type) -> AnyPublisher<ResultType, APIError> {
        
        sendRequest(request)
            .tryMap({ data in
                if let data = data {
                    return data
                } else {
                    throw AFError.responseSerializationFailed(reason: .inputDataNilOrZeroLength)
                }
            })
            .data(at: keypath)
            .decode(type: ResultType.self, decoder: decoder)
            .mapError { $0 as? APIError ?? APIError.other($0) }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    // MARK: - Optional Decodable
   
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible,
                                                         keypath: String?,
                                                         decoder: JSONDecoder,
                                                         decodedType: ResultType.Type) -> AnyPublisher<ResultType?, APIError> {
        
        sendRequest(request)
            .optionalData(at: keypath)
            .mapError { $0 as? APIError ?? APIError.other($0) }
            .flatMap({ optionalData -> AnyPublisher<ResultType?, APIError> in
                let resultPublisher: AnyPublisher<ResultType?, APIError>
                if let data = optionalData {
                    resultPublisher = Just(data)
                        .decode(type: ResultType.self, decoder: decoder)
                        .mapError { $0 as? APIError ?? APIError.other($0) }
                        .map { Optional.some($0) }
                        .eraseToAnyPublisher()
                } else {
                    resultPublisher = Just(nil)
                        .setFailureType(to: APIError.self)
                        .eraseToAnyPublisher()
                }
                return resultPublisher
                    .receive(on: DispatchQueue.main)
                    .eraseToAnyPublisher()
            })
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    // MARK: - Upload
    
    private func sendUploadRequest(_ uploadRequest: UploadRequest, progressHandler: @escaping Request.ProgressHandler) -> AnyPublisher<Data?, APIError> {
        Future { fullfill in
            uploadRequest
                .uploadProgress(closure: progressHandler)
                .validate()
                .response { response in
                    switch response.result {
                    case .success(let data):
                        fullfill(.success(data))
                    case .failure(let error):
                        let apiError = APIError(rootError: error, failedResponse: response.response, responseData: response.data)
                        fullfill(.failure(apiError))
                    }
                }
        }.eraseToAnyPublisher()
    }
    
    func uploadData(_ data: Data,
                    request: URLRequestConvertible,
                    progressHandler: @escaping Request.ProgressHandler) -> (UploadRequest, AnyPublisher<Data?, APIError>) {
        
        let uploadRequest = session.upload(data, with: request)
        let publisher = sendUploadRequest(uploadRequest, progressHandler: progressHandler)
        return (uploadRequest, publisher)
    }
    
    func multipartUploadData(_ multipartFormData: MultipartFormData,
                             request: URLRequestConvertible,
                             progressHandler: @escaping Request.ProgressHandler) -> (UploadRequest, AnyPublisher<Data?, APIError>) {
        
        let uploadRequest = session.upload(multipartFormData: multipartFormData, with: request)
        let publisher = sendUploadRequest(uploadRequest, progressHandler: progressHandler)
        return (uploadRequest, publisher)
    }
    
    // MARK: - Download
    
    func dowloadFile(at url: URL,
                     destination: DownloadRequest.Destination?,
                     progressHandler: @escaping Request.ProgressHandler) -> (DownloadRequest, AnyPublisher<URL, APIError>) {
        
        let downloadRequest = session.download(url, to: destination)
        let publisher = Future<URL, APIError> { fullfill in
            downloadRequest
                .downloadProgress(closure: progressHandler)
                .validate()
                .responseURL { (response) in
                    switch response.result {
                    case .success(let fileURL):
                        fullfill(.success(fileURL))
                    case .failure(let error):
                        let apiError = APIError(rootError: error, failedResponse: response.response, responseData: nil)
                        fullfill(.failure(apiError))
                    }
                }
        }.eraseToAnyPublisher()
        
        return (downloadRequest, publisher)
    }
}

// MARK: - SessionManagerProtocol extension

extension SessionManagerProtocol {
    
    // MARK: - Decodable
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible) -> AnyPublisher<ResultType, APIError> {
        requestDecodable(request, keypath: nil, decoder: JSONDecoder(), decodedType: ResultType.self)
    }
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String) -> AnyPublisher<ResultType, APIError> {
        requestDecodable(request, keypath: keypath, decoder: JSONDecoder(), decodedType: ResultType.self)
    }
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, decoder: JSONDecoder) -> AnyPublisher<ResultType, APIError> {
        requestDecodable(request, keypath: nil, decoder: decoder, decodedType: ResultType.self)
    }
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, decodedType: ResultType.Type) -> AnyPublisher<ResultType, APIError> {
        requestDecodable(request, keypath: nil, decoder: JSONDecoder(), decodedType: decodedType)
    }
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String, decoder: JSONDecoder) -> AnyPublisher<ResultType, APIError> {
        requestDecodable(request, keypath: keypath, decoder: decoder, decodedType: ResultType.self)
    }
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String, decodedType: ResultType.Type) -> AnyPublisher<ResultType, APIError> {
        requestDecodable(request, keypath: keypath, decoder: JSONDecoder(), decodedType: decodedType)
    }
    
    func requestDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, decoder: JSONDecoder, decodedType: ResultType.Type) -> AnyPublisher<ResultType, APIError> {
        requestDecodable(request, keypath: nil, decoder: decoder, decodedType: decodedType)
    }
    
    // MARK: - Optional Decodable

    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible) -> AnyPublisher<ResultType?, APIError> {
        requestOptionalDecodable(request, keypath: nil, decoder: JSONDecoder(), decodedType: ResultType.self)
    }
    
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String) -> AnyPublisher<ResultType?, APIError> {
        requestOptionalDecodable(request, keypath: keypath, decoder: JSONDecoder(), decodedType: ResultType.self)
    }
    
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, decoder: JSONDecoder) -> AnyPublisher<ResultType?, APIError> {
        requestOptionalDecodable(request, keypath: nil, decoder: decoder, decodedType: ResultType.self)
    }
    
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, decodedType: ResultType.Type) -> AnyPublisher<ResultType?, APIError> {
        requestOptionalDecodable(request, keypath: nil, decoder: JSONDecoder(), decodedType: decodedType)
    }
    
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String, decoder: JSONDecoder) -> AnyPublisher<ResultType?, APIError> {
        requestOptionalDecodable(request, keypath: keypath, decoder: decoder, decodedType: ResultType.self)
    }
    
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, keypath: String, decodedType: ResultType.Type) -> AnyPublisher<ResultType?, APIError> {
        requestOptionalDecodable(request, keypath: keypath, decoder: JSONDecoder(), decodedType: decodedType)
    }
    
    func requestOptionalDecodable<ResultType: Decodable>(_ request: URLRequestConvertible, decoder: JSONDecoder, decodedType: ResultType.Type) -> AnyPublisher<ResultType?, APIError> {
        requestOptionalDecodable(request, keypath: nil, decoder: decoder, decodedType: decodedType)
    }
}
