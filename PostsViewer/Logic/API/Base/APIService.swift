//
//  APIService.swift
//  ArchitectureExample
//
//  Created by Nick Sudibor on 15.02.21.
//

import Foundation

class BaseAPIService {
    let sessionManager: SessionManagerProtocol
    
    init(sessionManager: SessionManagerProtocol) {
        self.sessionManager = sessionManager
    }
}
