//
//  AuthAPIService.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 1.03.21.
//

import Foundation
import Combine

class AuthAPIService: BaseAPIService {
    func authenticate(login: String, password: String) -> AnyPublisher<AuthResult, APIError> {
        let result = AuthResult(token: "Mock Token")
        return Just(result)
            .delay(for: .seconds(0.5), scheduler: DispatchQueue.main)
            .setFailureType(to: APIError.self)
            .eraseToAnyPublisher()
//        let error = BasicError("Wrong Credentials")
//        let apiError = APIError.authentification(error)
//        return Fail(error: apiError)
//            .delay(for: .seconds(0.5), scheduler: DispatchQueue.main)
//            .eraseToAnyPublisher()
    }
}
