//
//  RequestsApiManager.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import Foundation
import Alamofire
import Combine

class PostsAPIService: BaseAPIService {
    func fetchPosts() -> AnyPublisher<[Post], APIError> {
        sessionManager.requestDecodable(APIRequest<NoParameters>(path: "posts"), decodedType: [Post].self)
    }
}
