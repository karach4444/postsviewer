//
//  AlbumsAPIService.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import Foundation
import Combine

class AlbumsAPIService: BaseAPIService {
    func fetchAlbums() -> AnyPublisher<[Album], APIError> {
        sessionManager.requestDecodable(APIRequest<NoParameters>(path: "albums"), decodedType: [Album].self)
    }
}
