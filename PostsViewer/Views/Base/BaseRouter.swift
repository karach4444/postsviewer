//
//  BaseRouter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 23.02.21.
//

import UIKit

class BaseRouter {
    private(set) weak var hostViewController: UIViewController?
    
    init(hostViewController: UIViewController?) {
        self.hostViewController = hostViewController
    }
}
