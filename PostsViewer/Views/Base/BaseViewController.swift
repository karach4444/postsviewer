//
//  BaseViewController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        localize()
    }
    
    func setupView() { }
    
    func localize() { }
}
