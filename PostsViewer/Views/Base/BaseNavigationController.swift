//
//  BaseNavigationController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
    }
}
