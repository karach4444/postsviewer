//
//  SceneDelegate.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    private var appManager: AppManagerProtocol!
    private var appFlowManager: AppFlowManager!
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        window.makeKeyAndVisible()
        appManager = AppManager()
        appFlowManager = AppFlowManager(authManager: appManager.authManager, router: AppFlowRouter(window: window, appManager: appManager))
        appFlowManager.openLoginScreen()
        appManager.authManager.autologin()
    }
}

