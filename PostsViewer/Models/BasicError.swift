//
//  BasicError.swift
//  ArchitectureExample
//
//  Created by Nick Sudibor on 15.02.21.
//

import Foundation

struct BasicError: LocalizedError {
    private(set) var errorDescription: String?
    
    init(_ errorDescription: String) {
        self.errorDescription = errorDescription
    }
}
