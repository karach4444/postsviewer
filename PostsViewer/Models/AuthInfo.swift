//
//  AuthInfo.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 1.03.21.
//

import Foundation

struct AuthInfo: Codable {
    let login: String
    let password: String
}
