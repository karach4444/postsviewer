//
//  Todo.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import Foundation

struct Todo: Codable {
    var userId: Int
    var id: Int
    var title: String
    var completed: Bool
}
