//
//  Post.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import Foundation

struct Post: Codable {
    var id: Int
    var title: String
    var body: String
    var userId: Int
}

