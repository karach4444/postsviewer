//
//  AuthResult.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 1.03.21.
//

import Foundation

struct AuthResult: Codable {
    let token: String
}
