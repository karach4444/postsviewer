// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum AlbumsList: StoryboardType {
    internal static let storyboardName = "AlbumsList"

    internal static let albumsTableViewController = SceneType<PostsViewer.AlbumsTableViewController>(storyboard: AlbumsList.self, identifier: "AlbumsTableViewController")
  }
  internal enum Home: StoryboardType {
    internal static let storyboardName = "Home"

    internal static let homeViewController = SceneType<PostsViewer.HomeViewController>(storyboard: Home.self, identifier: "HomeViewController")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Loading: StoryboardType {
    internal static let storyboardName = "Loading"

    internal static let loadingViewController = SceneType<PostsViewer.LoadingViewController>(storyboard: Loading.self, identifier: "LoadingViewController")
  }
  internal enum Login: StoryboardType {
    internal static let storyboardName = "Login"

    internal static let loginViewController = SceneType<PostsViewer.LoginViewController>(storyboard: Login.self, identifier: "LoginViewController")
  }
  internal enum Main: StoryboardType {
    internal static let storyboardName = "Main"
  }
  internal enum PostDetails: StoryboardType {
    internal static let storyboardName = "PostDetails"

    internal static let postDetailsViewController = SceneType<PostsViewer.PostDetailsViewController>(storyboard: PostDetails.self, identifier: "PostDetailsViewController")
  }
  internal enum PostsList: StoryboardType {
    internal static let storyboardName = "PostsList"

    internal static let postsTableViewController = SceneType<PostsViewer.PostsTableViewController>(storyboard: PostsList.self, identifier: "PostsTableViewController")
  }
  internal enum TodosList: StoryboardType {
    internal static let storyboardName = "TodosList"

    internal static let todosTableViewController = SceneType<PostsViewer.TodosTableViewController>(storyboard: TodosList.self, identifier: "TodosTableViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
