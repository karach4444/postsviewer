// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum Albums {
    /// Albums
    internal static let albums = L10n.tr("Localizable", "Albums.albums")
  }

  internal enum General {
    /// Internet connection lost
    internal static let connectionErrorDescription = L10n.tr("Localizable", "General.connectionErrorDescription")
    /// Error
    internal static let error = L10n.tr("Localizable", "General.error")
    /// Unknown Error
    internal static let generalErrorDescription = L10n.tr("Localizable", "General.generalErrorDescription")
    /// Ok
    internal static let ok = L10n.tr("Localizable", "General.ok")
  }

  internal enum Loading {
    /// Loading...
    internal static let loading = L10n.tr("Localizable", "Loading.loading")
  }

  internal enum Login {
    /// Login
    internal static let login = L10n.tr("Localizable", "Login.login")
    /// Password
    internal static let password = L10n.tr("Localizable", "Login.password")
    /// Username
    internal static let username = L10n.tr("Localizable", "Login.username")
  }

  internal enum Posts {
    /// Home
    internal static let home = L10n.tr("Localizable", "Posts.home")
    /// Logout
    internal static let logout = L10n.tr("Localizable", "Posts.logout")
    /// Post Details
    internal static let postDetails = L10n.tr("Localizable", "Posts.postDetails")
    /// Posts
    internal static let posts = L10n.tr("Localizable", "Posts.posts")
  }

  internal enum Todos {
    /// Todos
    internal static let todos = L10n.tr("Localizable", "Todos.todos")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
