//
//  String+Ext.swift
//  ArchitectureExample
//
//  Created by Nick Sudibor on 15.02.21.
//

import Foundation

extension String {
    func appendingPathComponent(_ component: String) -> String {
        return self.appending("/").appending(component)
    }
}
