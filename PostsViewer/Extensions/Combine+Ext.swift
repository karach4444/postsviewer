//
//  Combine+Ext.swift
//  ArchitectureExample
//
//  Created by Nick Sudibor on 15.02.21.
//

import Foundation
import Combine

extension Publisher {
    func subscribe(onValue: @escaping (Output) -> (), onFailure: ((Failure) -> ())?, onCompleted: (() -> ())? = nil) -> AnyCancellable {
        return sink(receiveCompletion: { (completion) in
            switch completion {
            case.finished:
                onCompleted?()
                break
            case .failure(let error):
                onFailure?(error)
            }
        }, receiveValue: { value in
            onValue(value)
        })
    }
    
    func subscribe(onValue: @escaping (Output) -> (), onCompleted: (() -> ())? = nil) -> AnyCancellable where Failure == Never {
        return sink(receiveCompletion: { completion in
            onCompleted?()
        }, receiveValue: { value in
            onValue(value)
        })
    }
}
