//
//  LoginViewController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 1.03.21.
//

import UIKit

protocol LoginViewProtocol: AnyObject {
    func startLoading()
    func finishLoading()
    func showErrorAlert(with error: Error)
}

class LoginViewController: BaseViewController {
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var passwordLabel: UILabel!
    @IBOutlet private weak var userNameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: LoginPresenterProtocol!
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        presenter.loadButtonPressedWith(login: userNameTextField.text ?? "", password: passwordTextField.text ?? "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
    }
    

    override func localize() {
        super.localize()
        userNameLabel.text = L10n.Login.username
        passwordLabel.text = L10n.Login.password
        loginButton.setTitle(L10n.Login.login, for: .normal)
        navigationItem.title = L10n.Login.login
    }
}

extension LoginViewController: LoginViewProtocol {
    func startLoading() {
        activityIndicator.startAnimating()
    }
    
    func finishLoading() {
        activityIndicator.stopAnimating()
        
    }
    
    func showErrorAlert(with error: Error) {
        let alertVC = UIAlertController(title: L10n.General.error, message: error.localizedDescription, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: L10n.General.ok, style: .cancel, handler: nil))
        present(alertVC, animated: true, completion: nil)
    }
}
