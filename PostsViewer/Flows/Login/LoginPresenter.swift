//
//  LoginPresenter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 1.03.21.
//

import Foundation
import Combine

protocol LoginPresenterProtocol {
    func loadButtonPressedWith(login: String, password: String)
    func attachView(_ view: LoginViewProtocol)
}

class LoginPresenter: LoginPresenterProtocol {
    private weak var view: LoginViewProtocol?
    private var authManager: AuthManagerProtocol
    private var subscriptions: Set<AnyCancellable>
    
    init(authManager: AuthManagerProtocol) {
        self.authManager = authManager
        self.subscriptions = []
    }
    
    func attachView(_ view: LoginViewProtocol) {
        self.view = view
    }
    
    func loadButtonPressedWith(login: String, password: String) {
        view?.startLoading()
        authManager.authenticate(login: login, password: password) {[weak self] result in
            switch result {
            case .success:
                self?.view?.finishLoading()
            case .failure(let error):
                self?.view?.finishLoading()
                self?.view?.showErrorAlert(with: error)
            }
        }
    }
}
