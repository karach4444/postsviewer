//
//  AlbumsListPresenter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import Foundation
import Combine

protocol AlbumsListPresenterProtocol {
    var numberOfAlbums: Int { get }
    func attachView(_ view: AlbumsListViewProtocol)
    func getAlbumStateForRow(at index: Int) -> AlbumCellStateModel
    func loadButtonDidTap()
}

class AlbumsListPresenter: AlbumsListPresenterProtocol {
    private let albumsAPIService: AlbumsAPIService
    private weak var view: AlbumsListViewProtocol?
    private var albums: [Album]
    private var subscriptions: Set<AnyCancellable>
    
    var numberOfAlbums: Int {
        albums.count
    }
    
    init(apiService: AlbumsAPIService) {
        self.albums = []
        self.albumsAPIService = apiService
        self.subscriptions = Set<AnyCancellable>()
    }
    
    func attachView(_ view: AlbumsListViewProtocol) {
        self.view = view
        loadButtonDidTap()
    }
    
    func getAlbumStateForRow(at index: Int) -> AlbumCellStateModel {
        AlbumCellStateModel(album: albums[index])
    }
    
    func loadButtonDidTap() {
        view?.startLoading()
        albumsAPIService
            .fetchAlbums()
            .subscribe { [weak self] albums in
                self?.albums = albums
                self?.view?.finishLoading()
                self?.view?.update()
            } onFailure: { [weak self] error in
                self?.view?.showError(error)
                self?.view?.finishLoading()
            }
            .store(in: &subscriptions)
    }
}
