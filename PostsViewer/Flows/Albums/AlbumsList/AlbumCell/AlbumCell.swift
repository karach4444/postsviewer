//
//  AlbumTableViewCell.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import UIKit

struct AlbumCellStateModel {
    let title: String
    
    init(album: Album) {
        self.title = album.title
    }
}

class AlbumCell: UITableViewCell, NibLoadableView {
    @IBOutlet private weak var titleLabel: UILabel!
    
    func updateWith(state: AlbumCellStateModel) {
        titleLabel.text = state.title
    }
}
