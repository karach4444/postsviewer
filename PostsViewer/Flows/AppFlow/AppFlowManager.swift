//
//  AppFlowManager.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 1.03.21.
//

import Foundation
import Combine

class AppFlowManager {
    private let authManager: AuthManagerProtocol
    private let router: AppFlowRouterProtocol
    private var subscriptions: Set<AnyCancellable>
    
    init(authManager: AuthManagerProtocol, router: AppFlowRouterProtocol) {
        self.authManager = authManager
        self.router = router
        self.subscriptions = []
        subscribeToAuthChanges()
    }
    
    func openLoginScreen() {
        router.openLoginPage()
    }
    
    private func subscribeToAuthChanges() {
        authManager
            .statePublisher
            .subscribe { [weak self] state in
                guard let self = self else { return }
                switch state {
                case .authenticated(_, let source):
                    switch source {
                    case .login, .autologin:
                        self.router.openAppTabBar()
                    default:
                        break
                    }
                case .notAuthentificated:
                    self.router.openLoginPage()
                case .refreshing, .initial:
                    self.router.openLoadingPage()
                }
            }
            .store(in: &subscriptions)
    }
}
