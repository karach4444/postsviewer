//
//  HomeRouter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 23.02.21.
//

import UIKit

protocol AppFlowRouterProtocol {
    func openAppTabBar()
    func openLoginPage()
    func openLoadingPage()
}

class AppFlowRouter: AppFlowRouterProtocol {
    private let window: UIWindow
    private let appManager: AppManagerProtocol
    
    init(window: UIWindow, appManager: AppManagerProtocol) {
        self.window = window
        self.appManager = appManager
    }
    
    private func createPostsFlow() -> UIViewController {
        let homeVC = StoryboardScene.Home.homeViewController.instantiate()
        homeVC.presenter = HomePresenter(router: HomeRouter(hostViewController: homeVC), authManager: appManager.authManager, apiService: PostsAPIService(sessionManager: appManager.sessionManager))
        let navigationController = BaseNavigationController(rootViewController: homeVC)
        return navigationController
    }
    
    private func createAlbumsFlow() -> UIViewController {
        let albumsVC = StoryboardScene.AlbumsList.albumsTableViewController.instantiate()
        albumsVC.presenter = AlbumsListPresenter(apiService: AlbumsAPIService(sessionManager: appManager.sessionManager))
        let navigation = BaseNavigationController(rootViewController: albumsVC)
        return navigation
    }
    
    private func createTodosFlow() -> UIViewController {
        let todosVC = StoryboardScene.TodosList.todosTableViewController.instantiate()
        todosVC.presenter = TodosListPresenter(apiService: TodosAPIService(sessionManager: appManager.sessionManager))
        let navigation = BaseNavigationController(rootViewController: todosVC)
        return navigation
    }
    
    private func createLoginFlow() -> UIViewController {
        let loginVC = StoryboardScene.Login.loginViewController.instantiate()
        loginVC.presenter = LoginPresenter(authManager: appManager.authManager)
        let navigation = BaseNavigationController(rootViewController: loginVC)
        return navigation
    }
    
    func openLoadingPage() {
        let loadingVC = StoryboardScene.Loading.loadingViewController.instantiate()
        changeRootController(to: loadingVC)
    }
    
    func openAppTabBar() {
        let tabBar = BaseTabBarController()
        let postsVC = createPostsFlow()
        let todosVC = createTodosFlow()
        let albumsVC = createAlbumsFlow()
        postsVC.tabBarItem = UITabBarItem(title: L10n.Posts.posts, image: nil, tag: 1)
        todosVC.tabBarItem = UITabBarItem(title: L10n.Todos.todos, image: nil, tag: 2)
        albumsVC.tabBarItem = UITabBarItem(title: L10n.Albums.albums, image: nil, tag: 3)
        tabBar.viewControllers = [postsVC, todosVC, albumsVC]
        changeRootController(to: tabBar)
    }
    
    func openLoginPage() {
        changeRootController(to: createLoginFlow())
    }
    
    private func changeRootController(to newController: UIViewController, animated: Bool = true) {
        if animated {
            UIView.transition(with: window, duration: 0.35, options: .transitionCrossDissolve) {
                let animationsState = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.window.rootViewController = newController
                UIView.setAnimationsEnabled(animationsState)
            }
        } else {
            window.rootViewController = newController
        }
    }
}
