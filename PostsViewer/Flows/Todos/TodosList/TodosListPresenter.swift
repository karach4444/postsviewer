//
//  TodosListPresenter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import Foundation
import Combine

protocol TodosListPresenterProtocol {
    var numberOfTodos: Int { get }
    func attachView(_ view: TodosListViewProtocol)
    func getTodoStateForRow(at index: Int) -> TodoCellStateModel
    func loadButtonDidTap()
}

class TodosListPresenter: TodosListPresenterProtocol {
    private let todosAPIService: TodosAPIService
    private weak var view: TodosListViewProtocol?
    private var todos: [Todo]
    private var subscriptions: Set<AnyCancellable>
    
    var numberOfTodos: Int {
        todos.count
    }
    
    init(apiService: TodosAPIService) {
        self.todosAPIService = apiService
        self.todos = []
        self.subscriptions = Set<AnyCancellable>()
    }
    
    func attachView(_ view: TodosListViewProtocol) {
        self.view = view
        loadButtonDidTap()
    }
    
    func getTodoStateForRow(at index: Int) -> TodoCellStateModel {
        TodoCellStateModel(todo: todos[index])
    }
    
    func loadButtonDidTap() {
        view?.startLoading()
        todosAPIService
            .fetchTodos()
            .subscribe { [weak self] todos in
                self?.todos = todos
                self?.view?.finishLoading()
                self?.view?.update()
            } onFailure: { [weak self] error in
                self?.view?.showError(error)
                self?.view?.finishLoading()
            }
            .store(in: &subscriptions)
    }
}
