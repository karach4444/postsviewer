//
//  TodoTableViewCell.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import UIKit

struct TodoCellStateModel {
    let title: String
    let completed: String
    
    init(todo: Todo) {
        self.title = todo.title
        self.completed = todo.completed.description
    }
}

class TodoCell: UITableViewCell, NibLoadableView {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var completedLabel: UILabel!
    
    func updateWith(state: TodoCellStateModel) {
        titleLabel.text = state.title
        completedLabel.text = state.completed
    }
}
