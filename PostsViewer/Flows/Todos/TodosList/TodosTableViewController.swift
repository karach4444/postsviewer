//
//  TodosTableViewController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 24.02.21.
//

import UIKit

protocol TodosListViewProtocol: AnyObject {
    func startLoading()
    func finishLoading()
    func showError(_ error: Error)
    func update()
}

class TodosTableViewController: BaseTableViewController {
    private weak var activityIndicator: UIActivityIndicatorView!
    var presenter: TodosListPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCellNib(ofType: TodoCell.self)
        presenter.attachView(self)
    }
    
    override func setupView() {
        super.setupView()
        let activity = UIActivityIndicatorView(style: .large)
        activity.frame = CGRect(x: 0, y: 0, width: 37, height: 37)
        activity.center = self.view.center
        view.addSubview(activity)
        self.activityIndicator = activity
        activity.color = .link
    }
    
    override func localize() {
        super.localize()
        navigationItem.title = L10n.Todos.todos
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfTodos
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: TodoCell.self, for: indexPath)
        cell.updateWith(state: presenter.getTodoStateForRow(at: indexPath.item))
        return cell
    }
}

extension TodosTableViewController: TodosListViewProtocol {
    func showError(_ error: Error) {
        let alertVC = UIAlertController(title: L10n.General.error, message: error.localizedDescription, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: L10n.General.ok, style: .cancel, handler: { (_) in
            self.presenter.loadButtonDidTap()
        }))
        present(alertVC, animated: true, completion: nil)
    }
    
    func startLoading() {
        activityIndicator.startAnimating()
    }
    
    func finishLoading() {
        activityIndicator.stopAnimating()
    }
    
    func update() {
        tableView.reloadData()
    }
}
