//
//  LoadingViewController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 2.03.21.
//

import UIKit

class LoadingViewController: BaseViewController {
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var loadingLabel: UILabel!
    
    override func localize() {
        super.localize()
        loadingLabel.text = L10n.Loading.loading
    }
}
