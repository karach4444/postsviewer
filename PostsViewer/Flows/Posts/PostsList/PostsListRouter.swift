//
//  PostsListRouter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 23.02.21.
//

import UIKit

protocol PostsListRouterProtocol {
    func showPost(post: Post)
}

class PostsListRouter: BaseRouter, PostsListRouterProtocol {
    func showPost(post: Post) {
        let postDetailsVC = StoryboardScene.PostDetails.postDetailsViewController.instantiate()
        postDetailsVC.presenter = PostDetailsPresenter(post: post)
        hostViewController?.navigationController?.pushViewController(postDetailsVC, animated: true)
    }
}
