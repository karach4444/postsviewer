//
//  PostsPresenter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import Foundation

protocol PostsListPresenterProtocol {
    var numberOfPosts: Int { get }
    func attachView(_ view: PostsListViewProtocol)
    func getPostStateForRowAt(index: Int) -> PostCellStateModel
    func didTapPostAt(index: Int)
}

class PostsListPresenter: PostsListPresenterProtocol {
    private var posts: [Post]
    private weak var view: PostsListViewProtocol?
    private let router: PostsListRouterProtocol
    
    init(posts: [Post], router: PostsListRouterProtocol) {
        self.posts = posts
        self.router = router
    }
    
    var numberOfPosts: Int {
        posts.count
    }
    
    func attachView(_ view: PostsListViewProtocol) {
        self.view = view
    }
    
    func getPostStateForRowAt(index: Int) -> PostCellStateModel {
        PostCellStateModel(post: posts[index])
    }
    
    func didTapPostAt(index: Int) {
        router.showPost(post: posts[index])
    }
}
