//
//  PostersTableViewController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import UIKit

protocol PostsListViewProtocol: AnyObject {
    func updateList()
}

class PostsTableViewController: BaseTableViewController {
    var presenter: PostsListPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCellNib(ofType: PostCell.self)
        presenter.attachView(self)
    }
    
    override func localize() {
        super.localize()
        navigationItem.title = L10n.Posts.posts
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfPosts
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: PostCell.self, for: indexPath)
        cell.setupCellWith(state: presenter.getPostStateForRowAt(index: indexPath.item))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didTapPostAt(index: indexPath.item)
    }
}

extension PostsTableViewController: PostsListViewProtocol {
    func updateList() {
        tableView.reloadData()
    }
}
