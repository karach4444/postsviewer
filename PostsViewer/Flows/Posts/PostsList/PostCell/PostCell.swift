//
//  PostsTableViewCell.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import UIKit

struct PostCellStateModel {
    let title: String
    let body: String
    
    init(post: Post) {
        self.title = post.title
        self.body = post.body
    }
}

class PostCell: UITableViewCell, NibLoadableView {
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var body: UILabel!
    
    func setupCellWith(state: PostCellStateModel) {
        title.text = state.title
        body.text = state.body
    }
    
}
