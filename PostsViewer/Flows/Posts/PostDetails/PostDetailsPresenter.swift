//
//  PostPresenter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 23.02.21.
//

import Foundation

protocol PostDetailsPresenterProtocol {
    func attachView(_ view: PostDetailsViewProtocol)
}

class PostDetailsPresenter: PostDetailsPresenterProtocol {
    private weak var view: PostDetailsViewProtocol?
    private var post: Post
    
    init(post: Post) {
        self.post = post
    }
    
    func attachView(_ view: PostDetailsViewProtocol) {
        self.view = view
        view.updateWith(post: post)
    }
}
