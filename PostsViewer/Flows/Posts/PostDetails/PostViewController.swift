//
//  PostViewController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 23.02.21.
//

import UIKit

protocol PostDetailsViewProtocol: AnyObject {
    func updateWith(post: Post)
}

class PostDetailsViewController: BaseViewController {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var bodyLabel: UILabel!
    
    var presenter: PostDetailsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
    }
}

extension PostDetailsViewController: PostDetailsViewProtocol {
    func updateWith(post: Post) {
        titleLabel.text = post.title
        bodyLabel.text = post.body
    }
}
