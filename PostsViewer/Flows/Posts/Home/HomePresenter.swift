//
//  HomePresenter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import Foundation
import Combine

protocol HomePresenterProtocol {
    func attachView(_ view: HomeViewProtocol)
    func loadButtonPressed()
    func logoutButtonPressed()
}

class HomePresenter: HomePresenterProtocol {
    private let apiService: PostsAPIService
    private weak var view: HomeViewProtocol?
    private let router: HomeRouterProtocol
    private var subscriptions: Set<AnyCancellable>
    private let authManager: AuthManagerProtocol
    
    init(router: HomeRouterProtocol, authManager: AuthManagerProtocol, apiService: PostsAPIService) {
        self.router = router
        self.apiService = apiService
        self.subscriptions = Set<AnyCancellable>()
        self.authManager = authManager
    }
    
    func attachView(_ view: HomeViewProtocol) {
        self.view = view
    }
    
    func loadButtonPressed() {
        view?.startLoading()
        apiService
            .fetchPosts()
            .subscribe { [weak self] posts in
                self?.view?.finishLoading()
                self?.router.openPostsList(posts: posts)
            } onFailure: { [weak self] error in
                self?.view?.finishLoading()
                self?.view?.showErrorAlert(with: error)
            }
            .store(in: &subscriptions)
    }
    
    func logoutButtonPressed() {
        authManager.logout()
    }
}
