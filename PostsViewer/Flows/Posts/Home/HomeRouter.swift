//
//  HomeRouter.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 23.02.21.
//

import UIKit

protocol HomeRouterProtocol {
    func openPostsList(posts: [Post])
}

class HomeRouter: BaseRouter, HomeRouterProtocol {
    func openPostsList(posts: [Post]) {
        let postsListVC = StoryboardScene.PostsList.postsTableViewController.instantiate()
        postsListVC.presenter = PostsListPresenter(posts: posts, router: PostsListRouter(hostViewController: postsListVC))
        hostViewController?.navigationController?.pushViewController(postsListVC, animated: true)
    }
}
