//
//  ViewController.swift
//  PostsViewer
//
//  Created by Anton Karachinskiy on 22.02.21.
//

import UIKit

protocol HomeViewProtocol: AnyObject {
    func startLoading()
    func finishLoading()
    func showErrorAlert(with error: Error)
}

class HomeViewController: BaseViewController {
    @IBOutlet private weak var loadButton: UIButton!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: HomePresenterProtocol!
    
    @IBAction private func loadButtonPressed() {
        presenter.loadButtonPressed()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
    }
    
    override func setupView() {
        super.setupView()
        setupLogoutButton()
    }
    
    override func localize() {
        super.localize()
        navigationItem.title = L10n.Posts.home
    }
    
    private func setupLogoutButton() {
        let logoutButton = UIBarButtonItem(title: L10n.Posts.logout, style: .plain, target: self, action: #selector(logoutButtonPressed))
        navigationItem.leftBarButtonItem = logoutButton
    }
    
    @objc private func logoutButtonPressed() {
        presenter.logoutButtonPressed()
    }
}

extension HomeViewController: HomeViewProtocol {
    func startLoading() {
        activityIndicator.startAnimating()
        loadButton.isEnabled = false
    }
    
    func finishLoading() {
        activityIndicator.stopAnimating()
        loadButton.isEnabled = true
    }
    
    func showErrorAlert(with error: Error) {
        let alertVC = UIAlertController(title: L10n.General.error, message: error.localizedDescription, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: L10n.General.ok, style: .cancel, handler: nil))
        present(alertVC, animated: true, completion: nil)
    }
}

